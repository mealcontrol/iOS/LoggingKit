import XCTest
@testable import LoggingKit
#if canImport(Combine)
import Combine
#endif

final class LoggingKitTests: XCTestCase {
    func testExample() {
        let buffer = TextBufferBox("")
        LoggingSystem.bootstrap(level: .trace, metadata: ["Some": "MTD.", "Another": "MTD."], buffer: buffer)
        let logger = Logger(for: Self.self)
        // Expect to see message
        logger.log(level: .trace, "My message")
        logger.log(level: .debug, "My message")
        logger.log(level: .info, "My message")
        logger.log(level: .notice, "My message")
        logger.log(level: .warning, "My message")
        logger.log(level: .error, "My message")
        logger.log(level: .critical, "My message", metadata: ["Some": "MTD", "Another": "MTD"])
        
        // Expect to see object dump
        logger.trace(.dump(Date()))
        logger.debug(.dump(Date()))
        logger.notice(.dump(Date()))
        logger.warning(.dump(Date()))
        logger.error(.dump(Date()))
        logger.info(.dump(Date()))
        logger.critical(.dump(Date()), metadata: ["Some": "MTD", "Another": "MTD"])
        
        print("Buffer content:")
        print(buffer.content)
        
        #if canImport(Combine)
        if #available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *) {
            var subscriptions = Set<AnyCancellable>()
            let box = Box<String>("")
            buffer.clear()
            buffer.publihser.sink { box.content = $0 }.store(in: &subscriptions)
            print("Published content:")
            // Expect to see message
            logger.log(level: .trace, "My message1")
            logger.log(level: .debug, "My message1")
            logger.log(level: .info, "My message1")
            logger.log(level: .notice, "My message1")
            logger.log(level: .warning, "My message1")
            logger.log(level: .error, "My message1")
            logger.log(level: .critical, "My message1", metadata: ["Some": "MTD", "Another": "MTD"])
            XCTAssertEqual(box.content, buffer.content)
        }
        #endif
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
