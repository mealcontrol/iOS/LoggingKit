//
//  Box.swift
//  Logging
//
//  Created by Maxim Krouk on 4/2/20.
//
// https://github.com/apple/swift-evolution/blob/master/proposals/0258-property-wrappers.md#ref--box

#if canImport(Combine)
import Combine
#endif

public typealias TextBufferBox = Box<String>

extension TextBufferBox {
    public convenience init() {
        self.init("")
    }
    
    public func clear() {
        self.wrappedValue = ""
    }
}

@propertyWrapper
public final class Box<Value> {
    #if canImport(Combine)
    public var wrappedValue: Value {
        willSet { if #available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *) { _publisher.send(newValue) } }
    }
    #else
    public var wrappedValue: Value
    #endif
    
    public var content: Value {
        get { wrappedValue }
        set { wrappedValue = newValue }
    }
    
    public convenience init(_ wrappedValue: Value) {
        self.init(wrappedValue: wrappedValue)
    }
    
    public init(wrappedValue: Value) {
        self.wrappedValue = wrappedValue
        #if canImport(Combine)
        if #available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *) {
            self.__publisher = PassthroughSubject<Value, Never>()
        } else {
            self.__publisher = 0
        }
        #endif
    }
    
    #if canImport(Combine)
    private let __publisher: Any
    @available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
    private var _publisher: PassthroughSubject<Value, Never> { __publisher as! PassthroughSubject<Value, Never> }
    @available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
    public var publihser: AnyPublisher<Value, Never> { _publisher.eraseToAnyPublisher() }
    #endif
    
    public var projectedValue: Ref<Value> { ref }
    public var ref: Ref<Value> {
        Ref<Value>(
            read: { self.wrappedValue },
            write: { self.wrappedValue = $0 }
        )
    }
}

@propertyWrapper
@dynamicMemberLookup
public struct Ref<Value> {
    public let read: () -> Value
    public let write: (Value) -> Void
    
    public var wrappedValue: Value {
        get { return read() }
        nonmutating set { write(newValue) }
    }
    
    public subscript<U>(dynamicMember keyPath: WritableKeyPath<Value, U>) -> Ref<U> {
        return Ref<U>(
            read: { self.wrappedValue[keyPath: keyPath] },
            write: { self.wrappedValue[keyPath: keyPath] = $0 })
    }
}
