//
//  Loggable.swift
//  Logging
//
//  Created by Maxim Krouk on 4/2/20.
//

/// This protocol provides shared static and instance loggers for your types
///
/// No implementation is needed
public protocol Loggable {}

// MARK: Module loggers
private var sharedLoggers: [ObjectIdentifier: Logger] = [:]
extension Loggable {
    /// Shared logger for the type
    ///
    /// Should not be overriden
    @inlinable public var logger: Logger { Self.logger }
    
    /// Shared logger for the type
    ///
    /// Should not be overriden
    public static var logger: Logger {
        let id = ObjectIdentifier(self)
        if let logger = sharedLoggers[id] {
            return logger
        } else {
            let logger = Logger(for: self)
            sharedLoggers[id] = logger
            return logger
        }
    }
}
