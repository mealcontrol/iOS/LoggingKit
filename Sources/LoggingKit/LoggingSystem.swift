//
//  LoggingSystem.swift
//  Logging
//
//  Created by Maxim Krouk on 4/2/20.
//

import Logging

extension LoggingSystem {
    /// Bootstraps the logging system
    public static func bootstrap(
        level: Logger.Level = .trace,
        metadata: Logger.Metadata = [:],
        logTransform: LogTransform? = nil
    ) -> TextBufferBox {
        let buffer = TextBufferBox()
        self.bootstrap(
            level: level,
            metadata: metadata,
            buffer: buffer,
            logTransform: logTransform
        )
        return buffer
    }
    
    /// Bootstraps the logging system
    public static func bootstrap(
        level: Logger.Level = .trace,
        metadata: Logger.Metadata = [:],
        buffer: TextBufferBox,
        logTransform: LogTransform? = nil
    ) {
        self.bootstrap { label in
            LogHandler(
                label: label,
                metadata: metadata,
                logLevel: level,
                buffer: buffer,
                logTransform: logTransform
            )
        }
    }
}
