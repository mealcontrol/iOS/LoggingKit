//
//  Logging+Extensions.swift
//  Logging
//
//  Created by Maxim Krouk on 3/31/20.
//

// MARK: - Logger

extension Logger {
    public init<T>(for type: T.Type) {
        self.init(label: String(reflecting: type) + ".logger")
    }
}

// MARK: - Logger.Message

extension Logger.Message {
    
    public init(_ message: String) {
        self.init(stringLiteral: message)
    }
    
    @inlinable public static func dump(_ objects: Any...) -> Self { dump(objects) }
    public static func dump(_ objects: [Any]) -> Self {
        .init(objects.map(dump(object:)).joined(separator: .newline))
    }
    
    private static func dump(object: Any) -> String {
        var output = ""
        Swift.dump(object, to: &output)
        return output
    }
    
}

// MARK: - Logger.Level

extension Logger.Level {
    @inlinable var emoji: String {
        switch self {
        case .trace    : return "💬"
        case .debug    : return "🔘"
        case .info     : return "💡"
        case .notice   : return "🔔"
        case .warning  : return "⚠️"
        case .error    : return "❌"
        case .critical : return "🆘"
        }
    }
}
