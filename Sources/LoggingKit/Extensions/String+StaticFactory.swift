//
//  String+StaticFactory.swift
//  Mealcontrol
//
//  Created by Maxim Krouk on 3/6/20.
//  Copyright © 2020 Maxim Krouk. All rights reserved.
//

extension String {
    static var whitespace: String { " " }
    static var newline: String { "\n" }
    static var empty: String { "" }
    static var tab: String { "\t" }
}
