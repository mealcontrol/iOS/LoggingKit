//
//  Logging.swift
//  Logging
//
//  Created by Maxim Krouk on 3/31/20.
//

import Foundation

#if canImport(Combine)
import Combine

@available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
private var _logHandlerPublisher = PassthroughSubject<String, Never>()
#endif

/// Transformation of log parameters to logger input buffer compatible data
///
/// Default implementation is called if this field is equal to nil.
///
/// Parameters:
/// - level: Log level of incoming log request.
/// - message: Message of incoming log request.
/// - label: Label for logger.
/// - handlerMetadata: handler's metadata.
/// - logMetadata: Metadata of incoming log request.
/// - file: File, where incoming log request was created.
/// - function: Function, where incoming log request was created.
/// - line: Line, where incoming log request was created.
public typealias LogTransform = (
    Logger.Level,
    Logger.Message,
    String,
    Logger.Metadata,
    Logger.Metadata?,
    String,
    String,
    UInt
) -> String

struct LogHandler: Logging.LogHandler {
    let label: String
    var metadata: Logger.Metadata
    var logLevel: Logger.Level
    var buffer: Box<String>?
    var logTransform: LogTransform?
    
    private let lock = Lock()

    #if canImport(Combine)
    @available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *)
    var publisher: AnyPublisher<String, Never> { _logHandlerPublisher.eraseToAnyPublisher() }
    #endif
    
    @inlinable init(
        label: String,
        metadata: Logger.Metadata = [:],
        logLevel: Logger.Level = .trace,
        buffer: Box<String>? = .none,
        logTransform: LogTransform? = .none
    ) {
        self.label = label
        self.metadata = metadata
        self.logLevel = logLevel
        self.buffer = buffer
        self.logTransform = logTransform
    }
    
    @inlinable func log(
        level: Logger.Level,
        message: Logger.Message,
        metadata: Logger.Metadata?,
        file: String,
        function: String,
        line: UInt
    ) {
        guard logLevel <= level else { return }
        let transform = logTransform ?? Self.makeMessage
        let message = transform(level, message, label, self.metadata, metadata, file, function, line)

        lock.withLock {
            #if canImport(Combine)
            if #available(iOS 13.0, OSX 10.15, tvOS 13.0, watchOS 6.0, *) {
                _logHandlerPublisher.send(message)
            }
            #endif
            
            if let box = buffer {
                box.wrappedValue.write(message)
            }
        }
    }
    
    subscript(metadataKey key: String) -> Logger.Metadata.Value? {
        get { metadata[key] }
        set { metadata[key] = newValue }
    }
    
}

// MARK: Default message

extension LogHandler {
    @inlinable static func makeMessage(
        level: Logger.Level,
        message: Logger.Message,
        label: String,
        handlerMetadata: Logger.Metadata,
        logMetadata: Logger.Metadata?,
        file: String,
        function: String,
        line: UInt
    ) -> String {
        let date = Date()
        let id = UUID()
        return
            """
            \(level.emoji) \(label) [\(level.rawValue.uppercased())]
            # startlog(\(id))
            # \(date)
            # \(file) on line \(line) in '\(function)'\(getMetadataIfPresent(handlerMetadata, logMetadata))
            # message:
            :»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»»:
            \(message.description)
            :««««««««««««««««««««««««««««««««««««««««««««:
            # endlog(\(id))\n
            """
    }
    
    @inlinable static func getMetadataIfPresent(
        _ handlerMetadata: Logger.Metadata,
        _ logMetadata: Logger.Metadata?
    ) -> String {
        var output = ""
        if !handlerMetadata.isEmpty {
            output.append("\n# logger.metadata\n")
            output.append(Logger.Message.dump(handlerMetadata).description)
        }
        
        if let metadata = logMetadata {
            output.append("\n# logging.metadata\n")
            output.append(Logger.Message.dump(metadata).description)
        }
        
        if output.hasSuffix("\n") { output.removeLast() }
        return output
    }
}
